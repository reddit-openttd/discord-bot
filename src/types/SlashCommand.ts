import { ChatInputCommandInteraction, InteractionResponse, SlashCommandBuilder } from "discord.js";
import { ServerGroup } from "./Config";

export interface SlashCommand {
  data: SlashCommandBuilder
  execute: (interaction: ChatInputCommandInteraction, groupConfig: ServerGroup) => Promise<unknown>,
  onlyAdmin: boolean // only these roles can run the command
}
