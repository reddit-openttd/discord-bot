export type ModeratorEventType = 'moderator:chat' | 'moderator:private-chat' | 'moderator:bannable-dynamic-ip-found'

export type FlagReason = 'sexual' | 'hate' | 'violence' | 'self-harm' | 'sexual/minors' | 'hate/threatening' | 'violence/graphic'
// "sexual": true,
// "hate": false,
// "violence": false,
// "self-harm": false,
// "sexual/minors": false,
// "hate/threatening": false,
// "violence/graphic": false
export type ModAction = 'none' | 'warn' | 'kick' | 'ban'
export type HttpServerHost = {
  url: string
}

export type ClientEvent = {
  clientId: number
}

export type ModeratedChatViolation = {
  violationCount: number
  action: ModAction
  name: string
  ip: string
  flags: FlagReason[]
}

export type ModeratedChat = ClientEvent & {
  violation?: ModeratedChatViolation
  message: string,
  money: number
}

export type BannableDynamicIP = ClientEvent & {
  reason: string
  originalName: string
  ipAddress: string
}

export type ModeratorEvent<T> = {
  serverHost: HttpServerHost,
  serverName: string,
  event: ModeratorEventType,
  data: T
}