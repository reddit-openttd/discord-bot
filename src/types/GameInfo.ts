
export type MapSize = '6' | '7' | '8' | '9' | '10' | '11' | '12' | '13'
export type LandscapeType = 'temperate' | 'arctic' | 'tropic' | 'toyland'
export type MapInfo = {
  name: string
  seed: number
  landscape: number
  startdate: number
  mapheight: number
  mapwidth: number
}
export type GameInfo = {
  dedicated: 0 | 1
  map: MapInfo
  name: string
  version: string
  date?: number
}