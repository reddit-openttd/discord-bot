export type Setting = {
  section: string
  setting: string
  value: string
}
export type SettingChange = Setting & {
  before: string
}
export type Client = {
  id: number,
  name: string,
  companyId: number,
  ip: string
}

export type Company = {
  id: number,
  color: string,
  name: string,
  incorporated: string,
  balance: number,
  loan: number,
  value: number,
  trains: number,
  roadVehicles: number,
  planes: number,
  ships: number,
  password: boolean
}