import { SelectMenuInteraction } from "discord.js";

export interface MenuHandler {
  name: string
  execute: (interaction: SelectMenuInteraction, ...args) => Promise<unknown>
}

