import { onBannableIPFound }  from '../services/rabbitmq'
import { getClients } from '../services/restmin'
import { Client } from '../types/OpenTTDApi'
import {sendMessage} from '../services/discord'
import {Config} from '../types/Config'
import { BannableDynamicIP, ModeratorEvent } from '../types/ModeratorEvent'
import { ActionRowBuilder, ButtonBuilder, ButtonStyle } from 'discord.js'

const modMessageHandler = (config: Config) => (event: ModeratorEvent<BannableDynamicIP>) => {
  const bannableDetail = event.data
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))
  if (!group) return

  return getClients(event.serverHost)
    .then((clients: Client[]) => {
      const client = clients.find(c => c.id === bannableDetail.clientId)
      const ban = new ActionRowBuilder()
        .addComponents(
          new ButtonBuilder()
            .setCustomId(`dynipban$${event.serverName}$${client.id}|${client.name}$${bannableDetail.originalName}$${bannableDetail.ipAddress}`)
            .setLabel('ban')
            .setStyle(ButtonStyle.Danger),
          new ButtonBuilder()
            .setCustomId(`dynipban-cancel$${event.serverName}$${client.id}|${client.name}`)
            .setLabel('He\s legit')
            .setStyle(ButtonStyle.Secondary),
        ) as any
      const msg = `${client?.name || bannableDetail.clientId} on ${event.serverName} has joined with an IP (${client?.ip || 'N/A'}) flagged for being dynamic. Possible user '${bannableDetail.originalName}' banned for: ${bannableDetail.reason}`
      const ctx = sendMessage(group.staffChannel, { 
        content: msg,  
        components: [ban] 
      })
    })
}

export const init = (config: Config) => {
  onBannableIPFound(modMessageHandler(config))
}
