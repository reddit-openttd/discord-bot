import { onChat as onOpenttdChat }  from '../services/rabbitmq'
import { getClients } from '../services/restmin'
import { Client } from '../types/OpenTTDApi'
import { Chat,  OpenTTDEvent } from '../types/OpenTTDEvent'
import { sendMessage} from '../services/discord'
import {Config} from '../types/Config'

const openttdHandler = (config: Config) => (event: OpenTTDEvent<Chat>) => {
  const chatDetail = event.data
  if (!chatDetail.message.startsWith(`${config.prefix}admin `)) return // ignore commands
  
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))
  if (!group) return
  return getClients(event.serverHost)
    .then((clients: Client[]) => {
      const requestClient = clients.find(c => c.id === chatDetail.clientId)!
      const message = `<@&${group.staffRole}> ${requestClient.name} Has requested an admin on ${event.serverName}: ${chatDetail.message.replace('!admin', '')}`

      sendMessage(group.staffChannel, message)
    })
}

export const init = (config: Config) => {
  onOpenttdChat(openttdHandler(config))
}
