import { onJoin }  from '../services/rabbitmq'
import { getClients, sendBroadcastMessage, sendPrivateMessage, sendRcon } from '../services/restmin'
import { ClientEvent, HttpServerHost, OpenTTDEvent } from '../types/OpenTTDEvent'
import {curry} from 'ramda'
import axios, { AxiosResponse } from 'axios'
import { Config, ServerGroup } from '../types/Config'
import { Client } from '../types/OpenTTDApi'
import { sendMessage } from '../services/discord'
import {countryListAlpha2} from '../helpers/country'

const contact = 'efessel@gmail.com'

// const testData:Record<string, string> = {
//   'efess-bad': '185.234.70.85',
//   'efess-warn': '80.95.113.185',
//   'efess-ok': '73.234.182.54'
// }

type GetIpResultError = {
  status: 'error'
  message: string
  result: number
  queryIP: string
}

type GetIpResultSuccess = {
  status: 'success'
  result: number
  queryIP: string
  queryFlags: null
  queryFormat: 'json'
  contact: string
  BadIp: number
  Country: string
}
type GetIpResult = GetIpResultError | GetIpResultSuccess
const getCoutryName = (code: string) => (countryListAlpha2 as any)[code] || code
const scannedIPs: Record<string, GetIpResult> = {}
const kickMessage = 'Sorry, connecting from a VPN or proxy is not allowed! Please disable any such software and try again. If you think this is an error, please contact us.'
const getIpResult = (ip: string) => {
  const url = `http://check.getipintel.net/check.php?ip=${ip}&format=json&oflags=bc&contact=${contact}`

  return axios.get(url)
    .then((response: AxiosResponse<GetIpResult>) => response.data)
    .catch(err => {  
      console.log('GetIpError: ' + err.message)
      return {status: 'error' as const, message: 'err.message', result: 0, queryIP: ip}
    })
}

const ok = (group: ServerGroup, event: OpenTTDEvent<ClientEvent>, client: Client, result: GetIpResultSuccess) => {
  // let message = `*** ${client.name} is joining from ${result.Country}. `
  // return sendBroadcastMessage(host, message)
}

const warn = (group: ServerGroup, event: OpenTTDEvent<ClientEvent>, client: Client, result: GetIpResultSuccess) => {
  let message = `*** ${client.name} MIGHT BE CONNECTING VIA A PROXY IN ${getCoutryName(result.Country)}. ${(result.result * 100).toFixed()} certainty.`
  if (result.BadIp) {
    message += ' Warning: Potential ISP blacklisted address!'
  }
  
  return sendMessage(group.staffChannel, message)
}


const rejectOutright = (group: ServerGroup, event: OpenTTDEvent<ClientEvent>, client: Client, result: GetIpResultSuccess) => { 
  const broacastMessage = `*** ${client.name} was trying to connect from a VPN or proxy, which is not allowed.`
  const staffMessage = `${client.name} was trying to connect from a VPN or proxy on ${event.serverName} from ${getCoutryName(result.Country)}, which is not allowed.`
  return  sendPrivateMessage(event.serverHost, client.id, kickMessage)
      .then(() => sendRcon(event.serverHost, `ban ${client.id} "${kickMessage}"` ))
      .then(() => sendBroadcastMessage(event.serverHost, broacastMessage))
      .then(() => sendMessage(group.staffChannel, staffMessage))
}

const onJoinHandler = (config: Config) => (event: OpenTTDEvent<ClientEvent>) => {
  const clientEvent = event.data
  if (clientEvent.clientId === 1) { // server/host is always 1
    return
  } 
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))
  if (!group) return
  if (!group.enabledFeatures['vpn-protection']) return 
  return getClients(event.serverHost)
    .then(clients => {
      let client = clients.find(c => c.id === clientEvent.clientId)!
      if (!client)  { throw new Error('Join event for missing client') }
      // TEST DATA
      // if (testData[client.name]) {
      //   client = {
      //     ...client, 
      //     ip: testData[client.name]
      //   }
      // }
      // // END TEST DATA
      
      if((group.vpnBypassIps || []).indexOf(client.ip) > -1) {
        console.log(`Bypassing VPN check for whitelisted IP ${client.ip}`)
        return {result: {result: 0, status: 'success'}, client}
      }
      const scannedIP = scannedIPs[client.ip]
      if (scannedIP) {
        return {result: scannedIP, client}
      }
      return getIpResult(client.ip)
        .then(result => {
          scannedIPs[client.ip] = result
          return {result, client}
        })
    })
    .then(({result, client}) => {
      if (result.status === 'error') {
        const error = result as GetIpResultError
        console.log(`Bad result (${error.result}) from getipintel for ip ${error.queryIP}: ${error.message}`)
        return
      }
      const success = result as GetIpResultSuccess
      switch(true) {
        case success.result > .99: return rejectOutright(group, event, client, success)
        case success.result > .95 || success.BadIp === 1: return warn(group, event, client, success)
        default: return ok(group, event, client, success)
      }
    })
    .catch(err => console.error(err))
}

export const init = (config: Config) => {
  onJoin(onJoinHandler(config))
}