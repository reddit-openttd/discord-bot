import {Config} from '../types/Config'

import * as relay from './relay'
import * as newgame from './newgame'
import * as admin from './admin'
import * as screenshots from './screenshots'
import * as vpn from './vpn'
import * as mod from './mod'
import * as console from './log'
import * as bannableDynamicIP from './bannable-dynamic-ip'


export const init = (config: Config) => {
  relay.init(config)
  newgame.init(config)
  admin.init(config)
  screenshots.init(config)
  vpn.init(config)
  mod.init(config)
  console.init(config)
  bannableDynamicIP.init(config)
}
