import { onModeratorChat, onModeratorChatPrivate, onChat as onOpenttdChat }  from '../services/rabbitmq'
import { getClients, sendBroadcastMessage } from '../services/restmin'
import { Client } from '../types/OpenTTDApi'
import { Chat, HttpServerHost, OpenTTDEvent } from '../types/OpenTTDEvent'
import { onReady, onChat as onDiscordChat, sendMessage, sendWebhookMessage, getWebHook, getOrCreateThreadID, sendWebhookThreadMessage } from '../services/discord'
import {Config, ServerDefinition, ServerGroup} from '../types/Config'
import { Message, ThreadChannel, Webhook } from 'discord.js'
import { ModeratedChat, ModeratorEvent } from '../types/ModeratorEvent'
import { groupBy, partition } from 'ramda'

type ServerNameId = `server:${string}`
type GroupNameId = `group:${string}`

const threadName = (group: ServerGroup, server: ServerDefinition) => server.relay?.threadName ||
   `Game bridge ${group.relay?.mode === 'create-single-thread' ? '' : server.name}`.trim()
const webHooks: Record<ServerNameId | GroupNameId, Webhook> = {}

const apiErrorHandler = (host: HttpServerHost) => (err: Error) => {
  console.log(`Error calling OpenTTD Api (restmin) URL ${host.url}: ` + err.message)
}

const formatDiscordMessage = (message: string) => {
  return message.replace(/@/g, '(@)')
}

const openttdHandler = (config: Config) => (event: ModeratorEvent<ModeratedChat>) => {
  const chatDetail = event.data
  if (chatDetail.message.startsWith(config.prefix)) return // ignore commands
  if (chatDetail.violation) return // ignore chats marked with language violation
  
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))
  if (!group) return
  if (!group.enabledFeatures['relay']) return

  return getClients(event.serverHost)
    .then((clients: Client[]) => {
      const server = group.servers.find(s => s.name === event.serverName)
      if (!server) return
      const requestClient = clients.find(c => c.id === chatDetail.clientId)
      if (!requestClient) return
      const openTTDMessage = `${event.serverName}> ${requestClient.name}: ${chatDetail.message}`
      // opinion - if relay is only going to a single server thread, we probably shouldn't 
      // broadcast to all servers. 
      // Also - if a serverdefinition has it's own channelId, then lets not.
      if (group.relay?.mode !== 'create-thread-per-server' && !server.publicChannel) {
        group.servers.forEach(server => {
          const apiHost = { url: server.apiHost }
          if (server.name !== event.serverName) {
            sendBroadcastMessage(apiHost, openTTDMessage)
              .catch(apiErrorHandler(apiHost))
          }
        })
      }
      
      const webhookId = server.publicChannel ? `server:${server.name}` : `group:${group.name}`
      const pubChannelId = server.publicChannel ? server.publicChannel : group.publicChannel
      
      const discordMessage = formatDiscordMessage(chatDetail.message)
      const webhook = webHooks[webhookId]

      if (webhook) {
        const avatarUrl = `https://www.ropenttd.com/avatar?name=` + encodeURIComponent(requestClient.name)
        const userName = group.relay?.mode === 'create-thread-per-server' 
          ?  requestClient.name
          : `[${event.serverName}] ${requestClient.name}`
        if (!group.relay?.mode || group.relay?.mode === 'channel') {
          sendWebhookMessage(webhook, userName, avatarUrl, `${discordMessage}`)
        } else {
            return getOrCreateThreadID(pubChannelId, threadName(group, server))
              .then(thread => {
                if (thread) {
                  sendWebhookThreadMessage(webhook, thread.id, userName, avatarUrl, `${discordMessage}`)
                }
              })
        }
        
      } else {
        sendMessage(pubChannelId, `[${event.serverName}] ${requestClient.name}: ${discordMessage}`)
      }
    })
}

const discordHandler = (config: Config) => (event: Message) => {
  if (!event.content || event.webhookId) return
  let servers: ServerDefinition[] = []

  const isThread = event.channel.isThread()
  if (isThread) {
    const thread = event.channel as ThreadChannel
    servers = config.groups
      .filter(group => 
        group.enabledFeatures['relay'] 
          && group.publicChannel === thread.parentId
          && (group.relay?.mode === 'create-thread-per-server' 
            || group.relay?.mode === 'create-single-thread'))
      .reduce((_servers, g) => {
        const _s = g.servers.find(s => threadName(g, s) === thread.name)
        return _s ? _servers.concat([_s]) : _servers
      }, [] as ServerDefinition[])
  } else { // channel
    const groupsWithChannelRelay = config.groups.filter(g => 
      g.enabledFeatures['relay'] && (!g.relay?.mode || g.relay?.mode === 'channel'))

    servers = groupsWithChannelRelay
      .filter(g => g.publicChannel === event.channelId)
      .flatMap(g => g.servers.filter(s => !s.publicChannel || s.publicChannel === event.channelId))
    
    servers = servers.concat(groupsWithChannelRelay.flatMap(g => 
      g.servers.filter(s => 
        !servers.some(_s => _s !== s) &&
        s.publicChannel === event.channelId)))
  }

  if (!servers.length) return

  const message = `Discord> ${event.author.username}: ${event.content}`
  servers.forEach(server => {
    const apiHost = { url: server.apiHost }
    sendBroadcastMessage({ url: server.apiHost }, message)
      .catch(apiErrorHandler(apiHost))
  })
}

export const init = (config: Config) => {
  const createWebHook = (channelId: string, name: string, type: 'group' | 'server') => {
    const webhookId = `${type}:${name}`

    return getWebHook(channelId, `openttdbot-relay-${webhookId}`)
      .then(webhook => {
        if (webhook) {
          console.log(`Found relay webhook for ${webhookId} attached to channel ${channelId}`)
          webHooks[webhookId] = webhook
        }
      })
  }
  onReady(() => {
    return Promise.all(
      config.groups
        .filter(g => g.enabledFeatures['relay'])
        .flatMap(group => {
          // need to make sure webhooks are created for either server definitions that specify
          const webHookPromises = []
          const [hasServerRelay, noServerRelay] = partition(s => !!s.publicChannel && s.publicChannel !== group.publicChannel, group.servers)
          if (noServerRelay.length > 0 && !!group.publicChannel) { // All servers have relay.
            webHookPromises.push(createWebHook(group.publicChannel, group.name, 'group'))
          }
          // Only create/get a webhook once for each unique channel id.
          const distinct = groupBy(s => s.publicChannel, hasServerRelay)
          return webHookPromises.concat(
            Object.keys(distinct)
              .map(pubChan => createWebHook(pubChan, distinct[pubChan][0].name, 'server')))
          
        })
    )
    .then(() => {
      onModeratorChat(openttdHandler(config))
      onDiscordChat(discordHandler(config))
    })
  })
}


