import { onNewGame , onConsole, onWelcome}  from '../services/rabbitmq'
import { getClients, getSetting, sendBroadcastMessage } from '../services/restmin'
import { Client, Setting } from '../types/OpenTTDApi'
import { Chat, ConsoleEvent, OpenTTDEvent } from '../types/OpenTTDEvent'
import {sendMessage} from '../services/discord'
import {Config} from '../types/Config'
import { GameInfo, LandscapeType, MapSize } from '../types/GameInfo'
import { EmbedBuilder } from 'discord.js'

let _welcomePending: Record<string, number> = {}
const newGameHandler = (event: OpenTTDEvent<null>) => {
  _welcomePending[event.serverName] = new Date().getTime()
}
const mapSize: Record<MapSize, string> = {'6': '64', '7': '128', '8': '256', '9':'512', '10': '1024', '11': '2048', '12': '4096', '13': '8192'}
const isNewGame = (serverName: string) => {
  if (_welcomePending[serverName] && _welcomePending[serverName] > 0) {
    const newGameEventTime = _welcomePending[serverName];
    _welcomePending[serverName] = 0
    const diff = new Date().getTime() - newGameEventTime
    return diff < (60 * 1000) // give it one minute to generate map and send welcome
  }
}

const welcomeHandler = (config: Config) => (event: OpenTTDEvent<GameInfo>) => {
  if (!isNewGame(event.serverName)) return
  const gameInfo = event.data
  const group = config.groups.find(g => g.servers.some(s => s.name === event.serverName))

  if (!group) return
  if (!group.enabledFeatures['auto-newgame-announce']) return

  const landscapeType: Record<LandscapeType, string> = {
    'temperate': 'Temperate',
    'arctic': 'Arctic',
    'tropic': 'Tropic',
    'toyland': 'Toyland'
  }
  
  const screenshots: Record<LandscapeType, string> = {
    'temperate': 'https://wiki.openttd.org/uploads/en/Manual/Temperatescreenshot.png',
    'arctic': 'https://wiki.openttd.org/uploads/en/Manual/Subarcticscreenshot.png',
    'tropic': 'https://wiki.openttd.org/uploads/en/Manual/Subtropicalscreenshot.png',
    'toyland': 'https://wiki.openttd.org/uploads/en/Manual/Toylandscreenshot.png'
  }

  const server = group.servers.find(s => s.name === event.serverName)!
  const serverHost = { url: server.apiHost }
  const playerRole = server.playerRole || group.playerRole
  const channelToNotify = server.publicChannel || group.publicChannel 
  
  return Promise.all([
    getSetting(serverHost, 'game_creation', 'starting_year'),
    getSetting(serverHost, 'network', 'restart_game_year'),
    getSetting(serverHost, 'game_creation', 'landscape'),
    getSetting(serverHost, 'game_creation', 'map_x'),
    getSetting(serverHost, 'game_creation', 'map_y')
  ]).then(([startYear, endYear, landscape, mapWidth, mapHeight]: [Setting, Setting, Setting, Setting, Setting]) => {
    const embed = new EmbedBuilder()
      .setColor(0x0099FF)
      .setTitle(`New game on ${event.serverName}`)
      .setDescription(gameInfo.name)
      .setThumbnail('https://wiki.openttd.org/static/img/layout/openttd-64.gif')
      .addFields(
        { name: 'Version', value: gameInfo.version },
        { name: 'Climate', value: landscapeType[landscape.value as LandscapeType] || '???', inline: true },
        { name: 'Game Length', value: `${startYear.value || '???'} - ${endYear.value || '???'}`, inline: true },
        { name: 'Map Size', value: `${mapSize[mapWidth.value as MapSize]}x${mapSize[mapHeight.value as MapSize]}`, inline: true }
      )
      .setImage(screenshots[landscape.value as LandscapeType])
      .setTimestamp()
      .setFooter({ text: `Hosted by the OpenTTD Community`, iconURL: 'https://wiki.openttd.org/static/img/layout/openttd-64.gif' });

      sendMessage(channelToNotify, {content: playerRole ? `<@&${playerRole}>` : '', embeds: [embed]})
  })
}


export const init = (config: Config) => {
  onNewGame(newGameHandler)
  onWelcome(welcomeHandler(config))
} 
