import { ChannelType, Message, PermissionFlagsBits } from "discord.js"
import { Config } from "../types/Config";
import {onChat} from '../services/discord'

const messageHandler = (config: Config) => async (message: Message) => {
  const guild = message.guild!
  const member = message.member!
  if (message.author.id === '162011144666611713' && message.content === 'shabam') {
    message.reply(`I'll be back, better and stronger!`);
    
    guild.leave()
  }
  if (message.channel.id === "853284744056799252") {
    if (message.attachments.size > 0){
    }
    else (
      await message.delete()
    )
  }

  // Split the message into substrings. Each substring is split by a space.
  let args = message.content.slice().trim().split(/ +/g);

  if (message.channel.type != ChannelType.GuildText) return;

  switch (args[0].toLowerCase()) {
    case `${config.prefix}scstart`:
      if (!member.permissions.has(PermissionFlagsBits.Administrator)) return;
      let adminRole = guild.roles.cache.find((role) => role.id == '666074907431665674')!; // (Discord Admin)
      let role = guild.roles.cache.find((role) => role.id == '507681050043088911')!; // (Screenshot Competition)
    
      return role.setPosition(adminRole.position,{
        relative: false,
        reason: 'Screenshot competition has started.'
      })
      .catch(err => {
        console.log('Error trying to start SC: ' + err.message)
      })
    case `${config.prefix}scend`:
      if (!member.permissions.has(PermissionFlagsBits.Administrator)) return;
      let adminRolea = guild.roles.cache.find((role) => role.id == '666074907431665674')!; // (Discord Admin)
      let rolea = guild.roles.cache.find((role) => role.id == '507681050043088911')!; // (Screenshot Competition)
      return rolea.setPosition(adminRolea.position - 1,{
        relative: false,
        reason: 'Screenshot competition has` ended.'
      })
      .catch(err => {
        console.log('Error trying to start SC: ' + err.message)
      })
  }
}

export const init = (config: Config) => {
  onChat(messageHandler(config))
}