import axios, { AxiosResponse } from 'axios'
import { Method } from 'axios'
import { GameInfo } from '../types/GameInfo'
import { Client, Company, Setting, SettingChange } from '../types/OpenTTDApi'
import { HttpServerHost } from '../types/OpenTTDEvent'


export const sendBroadcastMessage = (host: HttpServerHost, message: string) => {
  return post(host.url, '/client/message', {message})
} 

export const sendPrivateMessage = (host: HttpServerHost, clientId: number, message: string) => {
  return post(host.url,  `/client/${clientId}/message`, {message})
}

export const getClient = (host: HttpServerHost, clientId: number): Promise<Client> => {
  return get<Client>(host.url,  `/client/${clientId}`)
}

export const getClients = (host: HttpServerHost): Promise<Client[]> => {
  return get<Client[]>(host.url,  `/client`)
}

export const getCompanies = (host: HttpServerHost): Promise<Company[]> => {
  return get<Company[]>(host.url,  `/company`)
}

export const getGameInfo = (host: HttpServerHost):Promise<GameInfo> => {
  return get<GameInfo>(host.url,  `/game`)
}

export const getSetting = (host: HttpServerHost, section: string, setting: string):Promise<Setting> => {
  return get<Setting>(host.url,  `/game/setting/${section}/${setting}`)
}
export const setSetting = (host: HttpServerHost, section: string, setting: string, value: string):Promise<SettingChange> => {
  return post<SettingChange>(host.url,  `/game/setting/${section}/${setting}`, {value})
}

export const getNewGameSetting = (host: HttpServerHost, section: string, setting: string):Promise<Setting> => {
  return get<Setting>(host.url,  `/game/newgame/setting/${section}/${setting}`)
}
export const setNewGameSetting = (host: HttpServerHost, section: string, setting: string, value: string):Promise<SettingChange> => {
  return post<SettingChange>(host.url,  `/game/newgame/setting/${section}/${setting}`, {value})
}

export const sendRcon = (host: HttpServerHost, command: string): Promise<string> => {
  return post<string>(host.url,  `/rcon`, {command})
}

export const get = <T>(host: string, resource: string): Promise<T> => {
  return call<T>('GET', host, resource)
}
export const post = <T>(host: string, resource: string, data: any) => {
  return call<T>('POST', host, resource, data)
}

export const call = <T>(method: Method, host: string, resource: string, data?: any) => {
  return axios.request<unknown, AxiosResponse<T>>({
    url: `${host}/api${resource}`,
    method,
    data
  })
  .then(r => r.data)
  .catch(err => {
    console.log(err)
    throw err
  })
}