import events from 'events'
import { Client, GatewayIntentBits, GuildMemberRoleManager, InteractionType, Message, MessagePayload, MessageCreateOptions, Routes, TextChannel, Webhook, ThreadChannel, WebhookMessageCreateOptions} from 'discord.js'
import { Config } from '../types/Config';
import {init as initCommands} from '../commands'

const openttdLogo = 'https://wiki.openttd.org/static/img/layout/openttd-64.gif'

export type ChatMessage = {
  author: string,
  message: string
}

let client: Client
let config: Config

export const discordEmitter = new events.EventEmitter();

export const init = (_config: Config) => {
  config = _config
  client = new Client({intents:[
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.Guilds, 
    GatewayIntentBits.GuildMessages
  ]});

  initCommands(client, config)

  client.on('ready', () => {
    discordEmitter.emit('ready')
    console.log('Logged in!')
  })

  // run when a message is sent
  client.on('messageCreate', async (message: Message) =>  {
    if (message.author.id === config.clientId) {
      return
    }
    discordEmitter.emit('chat', message)
  })

  client.login(config.authToken);
}

export const getOrCreateThreadID = (channelId: string, threadName: string): Promise<ThreadChannel<boolean> | void> => {
  if (!client) return Promise.resolve()

  const channel = client.channels.cache.get(channelId) as TextChannel
  if (!channel) {
    console.log(`Channelid ${channelId} not found to retrieve thread`)
    return Promise.resolve()
  }
  return channel.threads.fetch()
    .then(resp => {
      const thread = resp.threads.find(t => t.name === threadName)
      if (thread) {
        return thread
      }
      return channel.threads.create({
        name: threadName,
        reason: 'In game chat'
      })
    })
}
export const sendMessage = (channelId: string, message: string | MessagePayload | MessageCreateOptions ) => {
  if (client) {
    const channel = client.channels.cache.get(channelId) as TextChannel
    if (channel) {
      channel.send(message)
    } else {
      console.log('Channel not found for id: ' + channelId)
    }
  } else {
    console.log('Client not initialized.')
  }
}

export const getWebHook = (channelId: string, webhookName: string): Promise<Webhook | void> => {
  if (client) {
    const channel = client.channels.cache.get(channelId) as TextChannel
    if (!channel) {
      console.log(`Channelid ${channelId} not found to retrieve webook`)
      return Promise.resolve()
    }
    return channel.fetchWebhooks()
      .then(webhooks => {
        const theRightOne = webhooks.find(webhook => webhook.name === webhookName)
        if (theRightOne)  {
          return theRightOne.edit({
            avatar: openttdLogo
          })
          .catch(err => {
            console.log('error trying to edit webhook')
            console.error(err)
          })
        }
        return channel.createWebhook({
          name: webhookName,
          avatar: openttdLogo,
        })
          .catch((err) => {
            console.log('error trying to create webhook')
            console.error(err)
          });
      
      })
  }
  return Promise.resolve()
}
export const sendWebhookMessage = (webhook: Webhook, user: string, avatarUrl: string | null, message: string)  => {
  let payload: WebhookMessageCreateOptions = {
    username: user,
    content: message
  }

  if (avatarUrl) {
    payload.avatarURL = avatarUrl
  }

  webhook.send(payload)
}
export const sendWebhookThreadMessage = (webhook: Webhook, threadId: string, user: string, avatarUrl: string | null, message: string)  => {
  const payload:WebhookMessageCreateOptions = {
    username: user,
    content: message,
    threadId
  }
  if (avatarUrl) {
    payload.avatarURL = avatarUrl
  }
  webhook.send(payload)
    .then(resp => {
    })
    .catch(ex => {
      debugger
    })
}

export const onReady = (handler: (event: void) => void) => {
  discordEmitter.on('ready', handler)
}

export const onChat = (handler: (event: Message) => void) => {
  discordEmitter.on('chat', handler)
}
