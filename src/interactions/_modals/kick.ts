import { CacheType, ModalSubmitInteraction } from "discord.js"
import { Config } from "../../types/Config"
import { ModalHandler } from "../../types/ModalHandler"
import { sendRcon } from "../../services/restmin"
import { EmbedBuilder } from "@discordjs/builders"

export const init = (config: Config): ModalHandler => {
  return {
    name: 'kick',
    execute: (modal: ModalSubmitInteraction, serverName: string) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))
      const server = group.servers.find(s => s.name === serverName)
      const playerIdField = modal.fields.getField("clientId") as any

      //const ip = modal.fields.getTextInputValue("ip")
      const reason = modal.fields.getTextInputValue("reason")

      const valToUse = playerIdField

      if (!valToUse) {
        return modal.reply(({
          embeds: [
            new EmbedBuilder()
              .setColor(1343)
              .setDescription('Must select a player or enter IP')
          ],
          ephemeral: true
        }))
      } else {
        console.log(`${modal.user.username } is kicking player ${valToUse} because "${reason}"`)
        return sendRcon({url: server.apiHost}, `kick ${valToUse}`)
          .then(resp => {
		        return modal.reply({ content: 'Player was kicked ' + resp, ephemeral: true});
          })
      }
    }
  }
}