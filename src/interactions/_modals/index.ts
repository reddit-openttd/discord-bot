import { ModalHandler } from '../../types/ModalHandler'
import {init as initKick} from './kick'
import {init as initBan} from './ban'
import {init as initMove} from './move'
import { Config } from '../../types/Config'

export const getModals = (config: Config): ModalHandler[] =>{
  return [
    initKick(config),
    initBan(config),
    initMove(config)
  ]
}