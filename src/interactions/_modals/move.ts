import { CacheType, ModalSubmitInteraction } from "discord.js"
import { Config } from "../../types/Config"
import { ModalHandler } from "../../types/ModalHandler"
import { sendRcon } from "../../services/restmin"
import { EmbedBuilder } from "@discordjs/builders"

export const init = (config: Config): ModalHandler => {
  return {
    name: 'move',
    execute: async (modal: ModalSubmitInteraction<CacheType>, [serverName]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))
      const server = group.servers.find(s => s.name === serverName)
      const clientId = (  modal as any).getSelectMenuValues("clientId")[0]
      const companyId = (modal as any).getSelectMenuValues("companyId")[0]

      //const ip = modal.fields.getTextInputValue("ip")
      //const reason = modal.fields.getTextInputValue("reason")

      //const valToUse = playerIdField

      // if (!valToUse) {
      //   return modal.reply(({
      //     embeds: [
      //       new EmbedBuilder()
      //         .setColor(1343)
      //         .setDescription('Must select a player or enter IP')
      //     ],
      //     ephemeral: true
      //   }))
      // } else {
        console.log(`${modal.user.username } is moving player ${clientId} to company "${companyId}"`)
        return sendRcon({url: server.apiHost}, `move ${clientId} ${companyId}`)
          .then(resp => {
            modal.channel.send()
		        return modal.reply({ content: 'Player was moved successfully: '+ resp});
          })
      // }
    }
  }
}