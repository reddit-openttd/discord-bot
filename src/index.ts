import dotenv from 'dotenv'
import * as rabbitmq from './services/rabbitmq'
import * as handlers from './handlers'
import * as discord from './services/discord'
import getConfig from './config'
import sanityCheck from './sanity'
import { Config } from './types/Config'

dotenv.config()

getConfig()
  .then((config: Config | undefined) => {

    const cfg = sanityCheck(config)
    
    discord.init(cfg)
    rabbitmq.init(cfg)
    handlers.init(cfg)
  })
  
