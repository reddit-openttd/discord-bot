import { ButtonInteraction, EmbedBuilder } from "discord.js";
import { Config } from "../../types/Config";
import { ButtonHandler } from "../../types/ButtonHandler"
import { getClients, sendRcon } from "../../services/restmin"
import { sendMessage } from "../../services/discord";

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'move',
    execute: (button: ButtonInteraction, [serverName, client, company]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const [clientId, clientName] = client.split('|')
      const [companyId, companyName] = company.split('|')
      const httpEndpoint = {url: server.apiHost}
      
      const message = `${button.user.username } moved (#${clientId}) ${clientName} to (#${companyId}) ${companyName} on ${serverName}`
      console.log(message)
      return sendRcon(httpEndpoint, `move ${clientId} ${companyId}`)
        .then(resp => {
          return button.reply({ content: 'move Successful: ' + resp, ephemeral: true})
            .then(() => sendMessage(group.staffChannel, message));
        })
    }
  }
}