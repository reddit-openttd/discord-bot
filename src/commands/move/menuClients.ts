import { ActionRowBuilder, SelectMenuBuilder, SelectMenuInteraction } from "discord.js"
import { Config } from "../../types/Config"
import { MenuHandler } from "../../types/MenuHandler"
import { EmbedBuilder } from "@discordjs/builders"
import { getCompanies } from "../../services/restmin"

export const init = (config: Config): MenuHandler => {
  return {
    name: 'move-client',
    execute: async (menu: SelectMenuInteraction, [serverName]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const client = menu.values[0]

      const httpEndpoint = {url: server.apiHost}
      const [id, name] = client.split('|')

      if (!id) {
        return menu.reply(({
          embeds: [
            new EmbedBuilder()
              .setColor(1343)
              .setDescription('Must select a Player')
          ],
          ephemeral: true
        }))
      } else {
        getCompanies(httpEndpoint)
          .then(companies => 
            companies.map(company => ({
              value:  `${company.id}|${company.name}`,
              label: `(#${company.id}) ${company.name}`
            }))
        )
        .then(options => {
          const row = new ActionRowBuilder()
            .addComponents(
              new SelectMenuBuilder() // We create a Select Menu Component
                .setCustomId(`move-company$${serverName}$${client}`)
                .setPlaceholder('To which company?')
                .addOptions(...[{
                  label: 'Spectators',
                  value: '255|Spectators'
                }].concat(options))
              ) as any
          
          return menu.reply({ content: 'Which Company?', components: [row], ephemeral: true })
            .then(reply => {
              return reply
            })
        })
      }
    }
  }
}