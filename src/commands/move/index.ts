import { Config } from '../../types/Config'
import {InteractionRegister} from '../'
import {init as initSlashCommand} from './slash-command'
import {init as initClientMenu} from './menuClients'
import {init as initCompanyMenu} from './menuCompanies'
import {init as initButton} from './button'

export const init = (config: Config, register: InteractionRegister) => {
  register('SlashCommand', initSlashCommand(config))
  register('Menu', initClientMenu(config))
  register('Menu', initCompanyMenu(config))
  register('Button', initButton(config))
}