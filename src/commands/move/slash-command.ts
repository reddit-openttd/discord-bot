import { ActionRowBuilder, ChatInputCommandInteraction, SelectMenuBuilder, SlashCommandBuilder } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand";
import { Config, ServerGroup } from "../../types/Config";
import { getClients, getCompanies } from "../../services/restmin";

export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: true,
    data: new SlashCommandBuilder()
      .setName('move')
      .addStringOption(option => 
        option
          .setName('server')
          .addChoices(...config.groups.flatMap(g => g.servers.map(s => ({value: s.name, name: s.name}))))
          .setRequired(true)
          .setDescription('Which server to send command to'))
      .setDescription('Move\'s a player to another company (or spectators)'),
    execute: (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      const serverName = interaction.options.getString('server');
      const server = group.servers.find(s => s.name === serverName)!
      if (!server) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }

      const httpEndpoint = {url: server.apiHost}
     
      return Promise.all([
        getClients(httpEndpoint),
        getCompanies(httpEndpoint)
      ]).then(([clients, companies]) => 
        clients
          .filter(client => client.name !== server.name)
          .map(client => {
            const company = client.companyId && client.companyId !== 255
              ? companies.find(comp => comp.id === client.companyId)!.name
              : 'Spectator'
            return ({
              value: `${client.id}|${client.name}`,
              label: `(#${client.id}) ${client.name} [${company}]`
            })
        })
      )
      .then(options => {
        if (!options.length) {
          return interaction.reply({ content: 'There are currently no players on this server.', ephemeral: true })
        }
        const row = new ActionRowBuilder()
          .addComponents(
            new SelectMenuBuilder()
              .setCustomId(`move-client$${server.name}`)
              .setPlaceholder('Select player to move...')
              .addOptions(options),
          ) as any
        
        return interaction.reply({ content: 'Which Company?', components: [row], ephemeral: true })
          .then(reply => {
            return reply
          })
      })
    },
  }
}