import { ActionRowBuilder, ButtonBuilder, ButtonStyle, CacheType, ModalSubmitInteraction, SelectMenuInteraction } from "discord.js"
import { Config } from "../../types/Config"
import { MenuHandler } from "../../types/MenuHandler"
import { EmbedBuilder } from "@discordjs/builders"

export const init = (_config: Config): MenuHandler => {
  return {
    name: 'move-company',
    execute: async (menu: SelectMenuInteraction, [serverName, client]) => {
      const company = menu.values[0]

      const [clientId, clientName] = client.split('|')
      const [companyId, companyName] = company.split('|')

      if (!companyId) {
        return menu.reply(({
          embeds: [
            new EmbedBuilder()
              .setColor(1343)
              .setDescription('Must select a Company')
          ],
          ephemeral: true
        }))
      } else {
        const row = new ActionRowBuilder()
          .addComponents(
            new ButtonBuilder()
              .setCustomId(`move$${serverName}$${client}$${company}`)
              .setLabel('Move')
              .setStyle(ButtonStyle.Danger),
          ) as any

          await menu.reply({ content: `Move (#${clientId}) ${clientName} to (#${companyId}) ${companyName} on ${serverName}?`, ephemeral: true, components: [row] });
      }
    }
  }
}