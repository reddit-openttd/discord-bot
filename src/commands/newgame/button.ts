import { ButtonInteraction, EmbedBuilder } from "discord.js";
import { Config } from "../../types/Config";
import { ButtonHandler } from "../../types/ButtonHandler"
import { sendRcon } from "../../services/restmin"
import { sendMessage } from "../../services/discord";

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'newgame',
    execute: (button: ButtonInteraction, [serverName]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const message = `${button.user.username} started a newgame on ${serverName} `
      console.log(message)
      return sendRcon({url: server.apiHost}, `newgame`)
        .then(resp => {
          return button.reply({ content: 'Game restarted: ' + resp, ephemeral: true})
            .then(() => sendMessage(group.staffChannel, message))
        })
    }
  }
}