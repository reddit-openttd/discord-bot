import { ActionRowBuilder, ButtonBuilder, ButtonStyle, ChatInputCommandInteraction, SelectMenuBuilder, SlashCommandBuilder } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand";
import { Config, ServerGroup } from "../../types/Config";

export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: true,
    data: new SlashCommandBuilder()
      .setName('newgame')
      .addStringOption(option => 
        option
          .setName('server')
          .addChoices(...config.groups.flatMap(g => g.servers.map(s => ({value: s.name, name: s.name}))))
          .setRequired(true)
          .setDescription('Which server to send command to'))
      .setDescription('Starts a new game'),
    execute: (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      const serverName = interaction.options.getString('server');
      const server = group.servers.find(s => s.name === serverName)!
      if (!server) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }
      const row = new ActionRowBuilder()
        .addComponents(
          new ButtonBuilder()
            .setCustomId(`newgame$${serverName}`)
            .setLabel('newgame')
            .setStyle(ButtonStyle.Danger),
        ) as any

      return interaction.reply({ content: `End the current game on ${serverName}?`, ephemeral: true, components: [row] });
    },
  }
}