import { ModalHandler } from '../types/ModalHandler'
import { MenuHandler } from '../types/MenuHandler'
import { ButtonHandler } from '../types/ButtonHandler'
import { SlashCommand } from '../types/SlashCommand'
import { Client, GuildMemberRoleManager, Routes } from 'discord.js'
import { Config } from '../types/Config'
import { REST } from '@discordjs/rest';

import {init as initAdminStatus} from './adminStatus'
import {init as initBan} from './ban'
import {init as initBanip} from './banip'
import {init as initBanlist} from './banlist'
// import {init as initMove} from './move'
import {init as initKick} from './kick'
import {init as initMove} from './move'
import {init as initNewgame} from './newgame'
import {init as initReset} from './reset'
import {init as initServers} from './servers'
import {init as initStatus} from './status'
import {init as initUnban} from './unban'
import {init as initBannableDynamicIp} from './bannable-dynamic-ip'

export type InteractionType = 'Modal' | 'SlashCommand' | 'Menu' | 'Button'
export type InteractionHandler = ModalHandler | SlashCommand | MenuHandler | ButtonHandler
export type InteractionRegister = (type: InteractionType, handler: InteractionHandler) => void

const interactions = {
  'SlashCommand': [] as SlashCommand[],
  'Menu': [] as MenuHandler[],
  'Button': [] as ButtonHandler[],
  'Modal': [] as ModalHandler[]
}

export const registerInteraction = (type: InteractionType, handler: InteractionHandler) => {
  interactions[type].push(handler as any)
}

export const init = (client: Client, config: Config) => {
  [
    initAdminStatus,
    initBan,
    initBanip,
    initBanlist,
    initKick,
    initMove,
    initNewgame,
    initReset,
    initStatus,
    initUnban,
    initServers,
    initBannableDynamicIp
  ].forEach(c => c(config, registerInteraction))

  const rest = new REST({ version: '10' }).setToken(config.authToken);
  rest.put(
    Routes.applicationGuildCommands(config.clientId, config.guildId),
    { body: interactions['SlashCommand'].map(c => c.data.toJSON()) },
  )
  .then(() => console.log('Registered commands'))
  .catch((err) => console.error('Error registering commands ' + err.message))
  
  client.on('interactionCreate', async interaction => {
    if (!interaction.isStringSelectMenu()) return;
    
    const [name, ...args] = interaction.customId.split('$')
    const menu = interactions['Menu'].find(c => c.name === name )
    if (!menu) return;

    try {
      await menu.execute(interaction, args)
    } catch (error) {
      console.error(error);
      try {
        await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
      } catch {}
    }
  })

  client.on('interactionCreate', async interaction => {
    if (!interaction.isButton()) return;
    
    const [name, ...args] = interaction.customId.split('$')
    const button = interactions['Button'].find(c => c.name === name )
    if (!button) return;

    try {
      await button.execute(interaction, args)
    } catch (error) {
      console.error(error);
      try {
        await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
      } catch {}
    }
  })
    
  client.on('modalSubmit', async (modal) => {
    const [name, ...args] = modal.customId.split('$')
    
    const modalHandler = interactions['Modal'].find(c => c.name === name )
    if (!modalHandler) return
    
    try {
      await modalHandler.execute(modal, args)
    } catch (error) {
      console.error(error)
      try {
        await modal.reply({ content: 'There was an error while executing this command!', ephemeral: true })
      } catch {}
    }
  })

  // client.on('interactionCreate',async interaction => {
  //   if (interaction.type !== InteractionType.ModalSubmit) return;
    
  //   const [name, ...args] = interaction.customId.split('$')
  //   const modal = modals.find(c => c.name === name )
  //   if (!modal) return;
    
  //   try {
  //     await modal.execute(interaction, args)
  //   } catch (error) {
  //     console.error(error);
  //     try {
  //       await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
  //     } catch {}
  //   }
  // })

  client.on('interactionCreate', async interaction => {
    if (!interaction.isChatInputCommand()) return;
    
    const userRoles = (interaction.member!.roles as GuildMemberRoleManager).cache;
    const command = interactions['SlashCommand'].find(c => c.data.name === interaction.commandName)
    if (!command) return;
    const group = config.groups.find(g => 
      g.publicChannel === interaction.channelId 
      || g.staffChannel === interaction.channelId 
      || g.servers.some(s => s.publicChannel == interaction.channelId ))
    // Allow to issue commands from any channel
    // if (!group) {
    //   console.error('Interaction from unknown group')
    //   return
    // }
    if (command.onlyAdmin) {
      if (!group || !group.adminRoles.some(role => userRoles.some(membRole => membRole.id === role))) {
        if (!group) {
          console.log(`${interaction.member!.user.username} attempted Admin command from unknown channel ${interaction.channelId}`)
        } else {
          console.log(`${interaction.member!.user.username} is not allowed to execute ${interaction.commandName}`)
        }
        try {
          await interaction.reply({ content: 'You don\'t have permission to run this command here.', ephemeral: true });
        } catch {}
        return
      }
    }

      
      
    try {
      await command.execute(interaction, group);
    } catch (error) {
      console.error(error);
      try {
        await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
      } catch {}
    }
  });
}