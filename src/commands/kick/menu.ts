import { ActionRowBuilder, ButtonBuilder, ButtonStyle, CacheType, ModalSubmitInteraction, SelectMenuInteraction } from "discord.js"
import { Config } from "../../types/Config"
import { MenuHandler } from "../../types/MenuHandler"
import { EmbedBuilder } from "@discordjs/builders"

export const init = (config: Config): MenuHandler => {
  return {
    name: 'kick',
    execute: async (menu: SelectMenuInteraction, [serverName]) => {
      const client = menu.values[0]

      const [id, name] = client.split('|')

      if (!id) {
        return menu.reply(({
          embeds: [
            new EmbedBuilder()
              .setColor(1343)
              .setDescription('Must select a Player')
          ],
          ephemeral: true
        }))
      } else {
        const row = new ActionRowBuilder()
          .addComponents(
            new ButtonBuilder()
              .setCustomId(`kick$${serverName}$${client}`)
              .setLabel('Kick')
              .setStyle(ButtonStyle.Danger),
          ) as any

          await menu.reply({ content: `Kick ${name} from ${serverName}?`, ephemeral: true, components: [row] });
      }
    }
  }
}