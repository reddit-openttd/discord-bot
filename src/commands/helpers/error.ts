import { AxiosError } from "axios"
import { ChatInputCommandInteraction } from "discord.js"

export const handle = (interaction: ChatInputCommandInteraction) => (err: Error | AxiosError) => {
  if ('code' in err && err.code === 'ENOTFOUND') {
    return interaction.reply({content: 'Server not found or is not responding', ephemeral: true})
  } else {
    return interaction.reply({content: 'General error: ' + err.message, ephemeral: true})
  }
}

export const handleDeferred = (interaction: ChatInputCommandInteraction) => (err: Error | AxiosError) => {
  if ('code' in err && err.code === 'ENOTFOUND') {
    return interaction.editReply({content: 'Server not found or is not responding'})
  } else {
    return interaction.editReply({content: 'General error: ' + err.message})
  }
}