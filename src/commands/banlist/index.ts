import { Config } from '../../types/Config'
import {InteractionRegister} from '../'
import {init as initSlashCommand} from './slash-command'

export const init = (config: Config, register: InteractionRegister) => {
  register('SlashCommand', initSlashCommand(config))
}