import { ActionRowBuilder, ButtonBuilder, ButtonStyle, ChatInputCommandInteraction, SelectMenuBuilder, SlashCommandBuilder } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand";
import { Config, ServerGroup } from "../../types/Config";

export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: true,
    data: new SlashCommandBuilder()
      .setName('banip')
      .addStringOption(option => 
        option
          .setName('server')
          .addChoices(...config.groups.flatMap(g => g.servers.map(s => ({value: s.name, name: s.name}))))
          .setRequired(true)
          .setDescription('Which server to send command to'))
      .addStringOption(option => 
            option
              .setName('ip')
              .setRequired(true)
              .setDescription('IP address of player to ban'))
      .setDescription('Bans a player!'),
    execute: (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      const serverName = interaction.options.getString('server');
      const ip = interaction.options.getString('ip')!
      const server = group.servers.find(s => s.name === serverName)
      if (!server) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }
      if (!/^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/.test(ip)) {
        return interaction.reply({ content: 'Ip is not in correct format', ephemeral: true})
      }
      const row = new ActionRowBuilder()
        .addComponents(
          new ButtonBuilder()
            .setCustomId(`banip$${serverName}$${ip}`)
            .setLabel('banip')
            .setStyle(ButtonStyle.Danger),
        ) as any

      return interaction.reply({ content: `Ban ${ip} from ${serverName}?`, ephemeral: true, components: [row] });
    },
  }
}