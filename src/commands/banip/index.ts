import { Config } from '../../types/Config'
import {InteractionRegister} from '../'
import {init as initSlashCommand} from './slash-command'
import {init as initButton} from './button'

export const init = (config: Config, register: InteractionRegister) => {
  register('SlashCommand', initSlashCommand(config))
  register('Button', initButton(config))
}