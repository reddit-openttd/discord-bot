import { ButtonInteraction, EmbedBuilder } from "discord.js";
import { Config } from "../../types/Config";
import { ButtonHandler } from "../../types/ButtonHandler"
import { sendRcon } from "../../services/restmin"
import { sendMessage } from "../../services/discord";

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'banip',
    execute: (button: ButtonInteraction, [serverName, ip]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const message = `${button.user.username } banned ${ip} from ${serverName} `
      console.log(message)
      return sendRcon({url: server.apiHost}, `ban ${ip}`)
        .then(resp => {
          return button.reply({ content: 'ban Successful: ' + resp, ephemeral: true})
            .then(() => sendMessage(group.staffChannel, message))
        })
    }
  }
}