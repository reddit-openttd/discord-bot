import { ChatInputCommandInteraction, EmbedBuilder, SelectMenuBuilder, SlashCommandBuilder } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand";
import { Config, ServerGroup } from "../../types/Config";
import { getClients, getCompanies, getGameInfo } from "../../services/restmin";

export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: true,
    data: new SlashCommandBuilder()
      .setName('admin-status')
      .addStringOption(option => 
        option
          .setName('server')
          .addChoices(...config.groups.flatMap(g => g.servers.map(s => ({value: s.name, name: s.name}))))
          .setRequired(true)
          .setDescription('Which server to send command to'))
      .setDescription('Shows status of all players and companies'),
    execute: (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      const serverName = interaction.options.getString('server');
      const server = group.servers.find(s => s.name === serverName)
      if (!server) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }

      const httpEndpoint = {url: server.apiHost}
     
      return Promise.all([
        getClients(httpEndpoint),
        getGameInfo(httpEndpoint),
        getCompanies(httpEndpoint)
      ]).then(([clients, gameInfo, companies]) => {
        const companiesAndSpectator = [
          ...companies,
          {id: 255, name: 'Spectators'}
        ]
        return companiesAndSpectator.flatMap((company) => {
          const clientsCompany = clients.filter(c => c.companyId === company.id)
          return {
              name: `(#${company.id}) ${company.name}, ${clientsCompany.length} connected clients`, 
              value: !clientsCompany.length
                ? '<empty>'
                : clientsCompany.map(client => `-  (#${client.id}) ${client.name} -  ${client.ip}`).join('\n')
            }
        })
      })
      .then(embedFields => {
          const embededContent = new EmbedBuilder()
          .setColor(0x0099FF)
          .setDescription(`Status of ${server.name}`)
          .setThumbnail('https://wiki.openttd.org/static/img/layout/openttd-64.gif')
          .addFields(embedFields)            
          .setTimestamp()
          .setFooter({ text: 'Hosted by the OpenTTD Community', iconURL: 'https://wiki.openttd.org/static/img/layout/openttd-64.gif' });

        return interaction.reply({
          embeds: [embededContent], ephemeral: true
        })
      })
    },
  }
}