import { ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand";
import { Config,ServerDefinition,ServerGroup } from "../../types/Config";
import { getClients } from "../../services/restmin";
import { getCompanies, getGameInfo, getSetting } from "../../services/restmin";
import { EmbedBuilder } from 'discord.js'
import {getDate, getGameDurationMs} from '../../helpers/date'
import {format} from 'date-fns'
import { handleDeferred } from "../helpers/error";
import humanizeDuration from 'humanize-duration'
import { LandscapeType, MapSize } from "../../types/GameInfo";

const mapSize: Record<MapSize, string> = {'6': '64', '7': '128', '8': '256', '9':'512', '10': '1024', '11': '2048', '12': '4096', '13': '8192'}
const landscapeType: Record<LandscapeType, string> = {
  'temperate': 'Temperate',
  'arctic': 'Arctic',
  'tropic': 'Tropic',
  'toyland': 'Toyland'
}

type RunningServer = {
  date: number,
  dayLength: string
  endYear: string,
  clients: string,
  maxClients: string,
  companies: string,
  maxCompanies: string,
  landscape: LandscapeType
  mapWidth: MapSize
  mapHeight: MapSize
  version: string,
  shortName: string,
  name: string,
  status: 'running'
}

type ServerDown = {shortName: string, status: 'down'}
type ServerStatus = RunningServer | ServerDown  
type ServersStatusCache = {
  time: number
  serverStatus: ServerStatus
}

const CACHE_TTL = 15 // seconds
const cache: Record<string, ServersStatusCache> = {}

const getEndsIn = (duration: number) => {
  const str = humanizeDuration(duration, { round: true }) as string
  return str.split(', ').slice(0, 2).join(', ')
}

const getServerStatus = (server: ServerDefinition) : Promise<ServerStatus> => {
  const serverHost = {url: server.apiHost}
  return Promise.all([
    getClients(serverHost),
    getCompanies(serverHost),
    getGameInfo(serverHost),
    getSetting(serverHost, 'game_creation', 'map_x'),
    getSetting(serverHost, 'game_creation', 'map_y'),
    getSetting(serverHost, 'game_creation', 'landscape'),
    getSetting(serverHost, 'network', 'restart_game_year'),
    getSetting(serverHost, 'network', 'max_clients'),
    getSetting(serverHost, 'network', 'max_companies'),
    getSetting(serverHost, 'economy', 'day_length_factor'),
  ]).then(([clients, companies, gameInfo, mapWidth, mapHeight, landscape, endYear, maxClients, maxCompanies, dayLength]) => ({
    date: gameInfo.date!,
    dayLength: dayLength.value,
    endYear: endYear.value,
    clients: clients.filter(client => client.ip !== 'server').length.toString(),
    maxClients: maxClients.value,
    companies: companies.length.toString(),
    maxCompanies: maxCompanies.value,
    landscape: landscape.value as LandscapeType,
    mapWidth: mapWidth.value as MapSize,
    mapHeight: mapHeight.value as MapSize,
    version: gameInfo.version,
    shortName: server.name,
    name: gameInfo.name,
    status: 'running' as const
  }))
}
export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: false,
    data: new SlashCommandBuilder()
      .setName('status')
      .addStringOption(option => 
        option
          .setName('server')
          .addChoices(...config.groups.flatMap(g => g.servers.map(s => ({value: s.name, name: s.name}))))
          .setRequired(true)
          .setDescription('Which server to send command to'))
      .setDescription('Shows the current status of the server'),
    execute: (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      const serverName = interaction.options.getString('server');
      const server = group.servers.find(s => s.name === serverName)
      if (!server) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }

      return interaction.deferReply({ ephemeral: true })
        .then(() => {
          const cacheKey = `${group.name}|${server.name}`
          if (cache[cacheKey] && cache[cacheKey].time + (CACHE_TTL * 1000) > Date.now())  {
            return cache[cacheKey].serverStatus
          } else {
            return getServerStatus(server)
              .then(serverStatus => {
                cache[cacheKey] = {
                  serverStatus,
                  time: Date.now()
                }
                return serverStatus
              })
          }
        })
        .then((status: ServerStatus) => {
          if (status.status !== 'running') {
             return interaction.editReply('Error retrieving server status')
          }
          const currentDate = format(getDate(status.date || 0), 'MMMM do, yyyy')
          let endText = '---'
          if (status.endYear && status.endYear !== '0') {
            const gameDuration = getGameDurationMs(status.date, parseInt(status.endYear), parseInt(status.dayLength || '1'))
        
            endText = `${status.endYear} (Ends in ${getEndsIn(gameDuration)})`
          }
          const embededContent = new EmbedBuilder()
            .setColor(0x0099FF)
            .setTitle(`Running game on ${server.name}`)
            .setDescription(status.name)
            .setThumbnail('https://wiki.openttd.org/static/img/layout/openttd-64.gif')
            .addFields(
              { name: 'Version', value: status.version },

              { name: 'Players', value: `${status.clients} / ${status.maxClients}`, inline: true },
              { name: 'Companies', value: `${status.companies} / ${status.maxCompanies}`, inline: true },
              { name: 'Current Date', value: `${currentDate}`, inline: true },

              { name: 'Climate', value: landscapeType[status.landscape], inline: true },
              { name: 'Map Size', value: `${mapSize[status.mapWidth]}x${mapSize[status.mapHeight]}`, inline: true },
              { name: 'Ends', value: endText, inline: true }
            )
            .setTimestamp()
            .setFooter({ text: 'Hosted by the OpenTTD Community', iconURL: 'https://wiki.openttd.org/static/img/layout/openttd-64.gif' });

          return interaction.editReply({
            embeds: [embededContent]
          })
        })
        .catch(handleDeferred(interaction))
    },
  }
}