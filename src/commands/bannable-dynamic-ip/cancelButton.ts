import { ButtonComponent, ButtonInteraction, EmbedBuilder } from "discord.js";
import { Config } from "../../types/Config";
import { ButtonHandler } from "../../types/ButtonHandler"
import { sendRcon } from "../../services/restmin"
import { sendMessage } from "../../services/discord";

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'dynipban-cancel',
    execute: (button: ButtonInteraction, [ serverName, client ]) => { 
      // TODO - whitelist user somehow?
      return button.update({
        content: `${button.message.content}\n--> ${button.user.username} marked legit.`,
        components: []
      })
        
    }
  }
}