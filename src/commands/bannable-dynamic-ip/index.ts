import { Config } from '../../types/Config'
import {InteractionRegister} from '../'
import {init as initBanButton} from './banButton'
import {init as initCancelButton} from './cancelButton'

export const init = (config: Config, register: InteractionRegister) => {
  register('Button', initBanButton(config))
  register('Button', initCancelButton(config))
}