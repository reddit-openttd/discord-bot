import { ButtonInteraction, EmbedBuilder } from "discord.js";
import { Config } from "../../types/Config";
import { ButtonHandler } from "../../types/ButtonHandler"
import { getClients, sendRcon } from "../../services/restmin"
import { sendMessage } from "../../services/discord";

export const init = (config: Config): ButtonHandler => {
  return {
    name: 'reset',
    execute: (button: ButtonInteraction, [serverName, company]) => {
      const group = config.groups.find(g => g.servers.some(s => s.name === serverName))!
      const server = group.servers.find(s => s.name === serverName)!
      const [_companyId, name] = company.split('|')
      const httpEndpoint = {url: server.apiHost}
      const companyId = parseInt(_companyId)
      
      return getClients(httpEndpoint)
        .then(clients => {
          const toDrop = clients.filter(c => c.companyId === companyId)
          // move to spectators
          return Promise.all(
            toDrop.map(client => sendRcon({url: server.apiHost}, `move ${client.id} 255`)) 
          )
        })
        .then(() => {
          const message = `${button.user.username} removed company ${name} (#${companyId}) from ${serverName}`
          console.log(message)
          return sendRcon({url: server.apiHost}, `reset_company ${companyId}`)
            .then(resp => {
              return button.reply({ content: 'reset Successful: ' + resp, ephemeral: true})
                .then(() => sendMessage(group.staffChannel, message))
            })
        })
    }
  }
}