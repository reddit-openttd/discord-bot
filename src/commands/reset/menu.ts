import { ActionRowBuilder, ButtonBuilder, ButtonStyle, CacheType, ModalSubmitInteraction, SelectMenuInteraction } from "discord.js"
import { Config } from "../../types/Config"
import { MenuHandler } from "../../types/MenuHandler"
import { EmbedBuilder } from "@discordjs/builders"

export const init = (_config: Config): MenuHandler => {
  return {
    name: 'reset',
    execute: async (menu: SelectMenuInteraction, [serverName]) => {
      const company = menu.values[0]

      const [id, name] = company.split('|')

      if (!id) {
        return menu.reply(({
          embeds: [
            new EmbedBuilder()
              .setColor(1343)
              .setDescription('Must select a Company')
          ],
          ephemeral: true
        }))
      } else {
        const row = new ActionRowBuilder()
          .addComponents(
            new ButtonBuilder()
              .setCustomId(`reset$${serverName}$${company}`)
              .setLabel('Reset')
              .setStyle(ButtonStyle.Danger),
          ) as any

          await menu.reply({ content: `Remove ${name} from ${serverName}?`, ephemeral: true, components: [row] });
      }
    }
  }
}