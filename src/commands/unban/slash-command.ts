import {  ChatInputCommandInteraction, SelectMenuBuilder, SlashCommandBuilder, TextInputBuilder } from "discord.js";
import { SlashCommand } from "../../types/SlashCommand";
import { Config,ServerGroup } from "../../types/Config";
import { sendRcon } from "../../services/restmin";
import * as error from '../helpers/error'
import { sendMessage } from "../../services/discord";

export const init = (config: Config): SlashCommand => {
  return {
    onlyAdmin: true,
    data: new SlashCommandBuilder()
      .setName('unban')
      .addStringOption(option => 
        option
          .setName('server')
          .addChoices(...config.groups.flatMap(g => g.servers.map(s => ({value: s.name, name: s.name}))))
          .setRequired(true)
          .setDescription('Which server to send command to'))
      .addStringOption(option => 
        option
          .setName('ip')
          .setRequired(true)
          .setDescription('Enter the IP of the player'))
      .setDescription('Removes a player from the ban list'),
    execute: (interaction: ChatInputCommandInteraction, group: ServerGroup) => {
      const serverName = interaction.options.getString('server');
      const server = group.servers.find(s => s.name === serverName)
      if (!server) {
        return interaction.reply({ content: 'Server was not found.', ephemeral: true})
      }

      const httpEndpoint = {url: server.apiHost}
      const ip = interaction.options.getString('ip');
      const message = `${interaction.user.username} un-banned player with ip ${ip}`
      console.log(message)
      return sendRcon(httpEndpoint, `unban ${ip}`)
        .then((response) => {
          return interaction.reply({ content: response, ephemeral: true })
            .then(() => sendMessage(group.staffChannel, message))
        })
        .catch(error.handle(interaction))

    }
  }
}