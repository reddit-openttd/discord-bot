const MS_PER_DAY =  86400000
const MS_PER_TICK = 30
const TICKS_PER_GAME_DAY = 74

const isLeapYear = (year: number) => {
  return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}

function dateFromDay(year: number, day: number){
  var date = new Date(year, 0); // initialize a date in `year-01-01`
  return new Date(date.setDate(day)); // add the number of days
}

export const getDate = (openttdDate: number) => {

  let rem
  let year = 0
  let day = 0
  /* There are 97 leap years in 400 years */
  year = (400 * Math.floor(openttdDate / (365 * 400 + 97)))
  rem  = (openttdDate % (365 * 400 + 97))

  /* There are 24 leap years in 100 years */
  year += (100 * Math.floor(rem / (365 * 100 + 24)));
  rem   =  (rem % (365 * 100 + 24));

  /* There is 1 leap year every 4 years */
  year += (4 * Math.floor(rem / (365 * 4 + 1)));
  rem  = (rem % (365 * 4 + 1));

  while (rem >= (isLeapYear(year) ? 366 : 365)) {
      rem -= isLeapYear(year) ? 366 : 365;
      year++;
  }

  day = ++rem;

  return dateFromDay(year, day)
}


export const getGameDurationMs = (currentDate: number, endYear: number, dayLenghtFactor: number) => {
  const endDate = new Date(endYear, 0)

  const _currentDate = getDate(currentDate)
  
  const inGameDays = (endDate.getTime() - _currentDate.getTime()) /  MS_PER_DAY
  const ticksToGo = inGameDays * TICKS_PER_GAME_DAY

  // there's 30 ms per tick
  // and 74 ticks per in game day

  return ticksToGo * MS_PER_TICK * dayLenghtFactor
}
    