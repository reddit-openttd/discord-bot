import { Config } from './types/Config'
import fs from 'fs'

export default (): Promise<Config | undefined> => {
  try {
    const config = require(process.env.CONFIG_PATH || '../config.json')
    if (config) {
      return Promise.resolve(config)
    }
  } catch{}

  return new Promise((resolve, reject) => {
    return fs.readFile(process.env.CONFIG_PATH || 'config.json', (err, data) => {
      if (err) reject(err)
      return resolve(data ? JSON.parse(data.toString()) as Config : undefined)
    })
  })
}