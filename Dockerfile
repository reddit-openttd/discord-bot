FROM node:gallium as build

RUN mkdir -p /usr/build

COPY . /usr/build

WORKDIR /usr/build

RUN npm ci && npm run build
COPY package.json /usr/build/dist
RUN cd dist && npm install --only=production

FROM node:gallium

RUN mkdir -p /usr/app

# Set the working directory to /app
WORKDIR /usr/app

COPY --from=build /usr/build/dist /usr/app

# Make game port available to the world outside this container
EXPOSE 80

# Define environment variable
# ENV NAME World

ENTRYPOINT ["node", "index.js"]
